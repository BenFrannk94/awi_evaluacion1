//expresiones
const expresiones = {
    codi: /^[a-zA-Z0-9]{1,5}$/, // alfanumérico de 5 caracteres.
    marc: /^[a-zA-Z0-9]{1,50}$/, // alfanumérico de 50 caracteres.
    model: /^[a-zA-Z0-9]{1,30}$/, // alfanumérico de 30 caracteres.
    añ: /^\d{4}$/ // numérico de 4 dígitos.
};

const formulario = document.getElementById("form");
const listInsputs = document.querySelectorAll(".form-input");

//validar formulario
form.addEventListener("submit", (e) => {
    //variables
    e.preventDefault();
    let condicion = validaciondatos();
    if(condicion){
        enviarFormulario();
    }
    
});

//funciones
function validaciondatos() {
    formulario.lastElementChild.innerHTML = "";
    let condicion = true;
    listInsputs.forEach(element => {
        element.lastElementChild.innerHTML= "";
    });
//condiciones
    var codigo = document.getElementById("codigo").value;
    if(expresiones.codi.test(codigo) == false) {
        muestraError("codigo", "Codigo no válido*");
        condicion = false; 
    }

    var marca = document.getElementById("marca").value;
    if(expresiones.marc.test(marca) == false) {
        muestraError("marca", "Marca no válido*");
        condicion = false; 
    }


    var modelo = document.getElementById("modelo").value;
    if(expresiones.model.test(modelo) == false) {
        muestraError("modelo", "Modelo no válido*");
        condicion = false; 
    }


    var año = document.getElementById("año").value;
    if(expresiones.añ.test(año) == false) {
        muestraError("año", "Año no válido*");
        condicion = false; 
    }

    var fini = document.getElementById("finicio").value;
    var ffin = document.getElementById("ffinal").value;
    if (fini > ffin) {
        muestraError("ffinal", "La fecha de inicio debe ser menor a la final*");
        condicion = false; 
    }

    
    return condicion;
}
//funcion error
function muestraError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
}
//funcion vaciar
function enviarFormulario(){
    formulario.reset();
    formulario.lastElementChild.innerHTML = "¡Gracias por su Registro!";
    setTimeout(() => {
        formulario.lastElementChild.innerHTML = "";
      }, 2000);
    
}